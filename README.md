# Cool Data Visualizations
*Czechapalooza 2018*

### Data Sets
 - **categories** - data usage by category / subcategory / app
 -- tree.json - everything combined and nested
 -- categories.json - parent list with categories
 -- categories/Adult.json etc - list of subcategories by category
 --- categories/rules/Banking.json etc - list of apps per subcategory

 - **secure** - threats by category / subject
 -- tree.json - everything combined and nested
 -- threatReportCategories.json - parent list with categories
 -- subjects/JAILBREAK.json etc - list of subjects by category

- **roaming**
-- locations.json - usage by location
-- breakdown.json - roaming devices

### Structure of the tree files
```
[
    {
        "label": "Branch A",
        "nodes": [
            {
                "label": "SubBranch B",
                "nodes": [
                    {
                        "label": "Leaf C"
                    }
                ]
            }
        ]
    }
]
```

### Chart Inspiration
##### Treemap of data usage
http://mbostock.github.io/d3/talk/20111018/treemap.html
https://github.com/kuy/treemap-with-router

##### Threats Categories Sunburst
https://www.jasondavies.com/coffee-wheel/
https://bl.ocks.org/mbostock/4348373

##### Threat Subjects Word Cloud
https://www.jasondavies.com/wordcloud/
https://github.com/jasondavies/d3-cloud

##### Better Roaming Map
(can be Highcharts)
use fill opacity for usage https://vida.io/gists/ot4Ynw4gZdmKkofo8
combine locations & device breakdown?

##### Sankey
https://bl.ocks.org/wvengen/cab9b01816490edb7083
https://www.infocaptor.com/sankey-diagram-software.php


#### More...
https://github.com/d3/d3/wiki/Gallery




